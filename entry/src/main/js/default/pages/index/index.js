import prompt from '@system.prompt';
export default {
    data: {
        title: "",
        imageList:[
            {id:1,url:"common/images/image1.jpeg"},
            {id:2,url:"common/images/image2.jpeg"},
            {id:3,url:"common/images/image3.jpeg"},
            {id:4,url:"common/images/image4.jpeg"},
            {id:5,url:"common/images/image5.jpeg"}
        ],
        change:1,
        index:0
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    swipeTo(){
        prompt.showToast({
            message:"获得子组件的值为："+this.$child("banner").change,
            duration:3000
        })
        this.$child("banner").swipeTo(0);
    },
    changeValue(){
        this.change +=1;
    },
    getPicIndex(e){
        this.index = e.detail.index;
    }

}
