export default {
    data() {
        return {
            title: ""
        }
    },
    props:['index'],
    onInit() {
        this.title = this.$t('strings.world');
    }
}
