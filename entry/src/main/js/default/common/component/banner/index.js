export default {
    data() {
        return {
            title: ""
        }
    },
    props:{
        imageList:{default:[]},
        duration:{default:500},
        change:{default:0}
    },
    onInit() {
        this.$watch("change","getChangeValue")
    },
    swipeTo(index){
        this.$element("swiper").swipeTo({ index: index });
    },
    getChangeValue(newValue,oldValue){
        console.log(`接收到的参数***new=${newValue}***old=${oldValue}`)
    },
    getPicIndex(e){
        this.$emit("getPicIndex",{index:e.index})
    }
}
