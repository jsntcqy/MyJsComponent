# MyJsComponent


#### 效果演示
![效果演示](https://images.gitee.com/uploads/images/2021/0826/160959_e23ee706_8627638.gif "js.gif")

#### 介绍
这里所有讲解均以一个banner自定义组件为例，主要介绍如何编写自定义组件并引用，以及如何去暴露组件的事件、属性，让组件使用起来方便、快捷。这里不再介绍如何搭建项目框架，我们从具体组件编写开始。  
需要注意的是，本编文章主要是在讲如何如何创建自定义组件，以及自定义组件与父组件之间的通信、方法暴露、事件暴露等等，组件实现方式某些地方并不合理，最好不要直接使用，即使使用也需要修改。

#### 组件的建立
首先，我们考虑将组件放在哪个文件夹。利用ide新建完项目后，entry/src/main/js/default下面为我们编写项目js代码的地方，一般情况下我们把组件放在common文件夹下，由于一个项目会有多个自定义组件，所以我们一般会在common下建立一个
component文件，下面放各种类型的自定义组件，具体文件路径如下图：  
![图](https://images.gitee.com/uploads/images/2021/0826/161018_6e1b63a0_8627638.png "图1.png")  
组件下面的css、hml、js文件，我们可以把pages/index下面的三个文件拷贝过去；也可以在pages文件夹右击-新建JS Page-输入组件名称-点击Finish后新建一个页面，将整个页面文件
夹拷贝到common/component文件夹下，再删除新建的页面即可。

#### 组件的使用
在页面hml文件中使用element标签引用,其name属性设置的值，可以在页面绘制当中当标签名称使用，具体代码如下：
```html
<element src="../../common/component/banner/index.hml" name="banner" ></element>
<element src="../../common/component/circle/index.hml" name="circle" ></element>
<div class="container">
    <banner></banner>
    <circle></circle>
</div>
```

#### 父组件给子组件传值
1、父组件中需要给组件加入属性并从传入属性值，例如：pages/index/index.hml中”image-list="{{imageList}}"“
```html
<banner id="banner" image-list="{{imageList}}" duration="1000" change="{{change}}" @get-pic-index="getPicIndex"></banner>
```
2、子组件中接收参数，common/component/banner/index.js中，属性值可以当data的值一样使用但是不可以在子组件中修改其值，js中使用采用”this.属性名“，hml中使用采用”{{ 属性名 }}“。
```javascript
props:{
        imageList:{default:[]},
        duration:{default:500},
        change:{default:0}
    }
```
这里需要注意下属性名称，如果全部小写，父子组件使用时均使用相同的单词即可；如果中间有大写字母，父组件使用时需要将大写字母变成小写字母，并且中间用”-“隔开。例如，属性”imageList“，父组件传值时使用”image-list“。
 
3、子组件中可以监控父组件传进来的属性数据改变使用方式是”this.$watch("change","getChangeValue")“,这样每次change属性有改变均会调用getChangeValue方法。
```javascript
onInit() {
        this.$watch("change","getChangeValue")
    }
getChangeValue(newValue,oldValue){
        console.log(`接收到的参数***new=${newValue}***old=${oldValue}`)
    }
```
每次属性改变，运行的log如下：  
&nbsp;&nbsp;&nbsp;&nbsp; app Log: 接收到的参数\*\*\*new=2\*\*\*old=1  
&nbsp;&nbsp;&nbsp;&nbsp; app Log: 接收到的参数\*\*\*new=3\*\*\*old=2  
&nbsp;&nbsp;&nbsp;&nbsp; app Log: 接收到的参数\*\*\*new=4\*\*\*old=3  
&nbsp;&nbsp;&nbsp;&nbsp; app Log: 接收到的参数\*\*\*new=5\*\*\*old=4  
&nbsp;&nbsp;&nbsp;&nbsp; app Log: 接收到的参数\*\*\*new=6\*\*\*old=5  

#### 父组件调用子组件方法
1、子组件存在的方法，如banner组件中swipeTo方法，让轮播跳转到指定位置图片：
```javascript
swipeTo(index){
        this.$element("swiper").swipeTo({ index: index });
    }
```
2、父组件中调用，使用”this.$child("banner")“取到子组件并调用其存在的方法：
```javascript
this.$child("banner").swipeTo(0);
```

#### 子组件调用父组件方法
1、子组件中注册事件方法，同时根据事件名称调用父组件的方法。具体实现，我们采用”this.$emit“，第一个参数事件名称，第二个参数为子组件传给父组件的值。
```javascript
getPicIndex(e){
        this.$emit("getPicIndex",{index:e.index})
    }
```
2、父组件中实现子组件注册的方法。  
1） htm将事件方法传入到子组件，使用组件时传入”@get-pic-index="getPicIndex"“。
```html
<banner id="banner" image-list="{{imageList}}" duration="1000" change="{{change}}" @get-pic-index="getPicIndex"></banner>
```
2） js实现传入的getPicIndex方法。
```javascript
getPicIndex(e){
        this.index = e.detail.index;
    }
```
这里需要注意下事件方法名称，如果全部小写，父子组件使用时均使用相同的单词即可；如果中间有大写字母，父组件使用时需要将大写字母变成小写字母，并且中间用”-“隔开。例如，事件方法”getPicIndex“，父组件传值时使用”get-pic-index“。


#### 子组件给父组件传值
这个传值，其他可以依赖于子组件调用父组件方法实现。因为注册事件方法时，第二个参数即为传入的值。  
1、子组件传入值”{index:e.index}“
```javascript
getPicIndex(e){
        this.$emit("getPicIndex",{index:e.index})
    }
```

2、父组件取出使用”e.detail.index“，其中”detail“是固定的，”index“为你传入的json串对象名称。
```javascript
getPicIndex(e){
        this.index = e.detail.index;
    }
```

3、另外说明下，使用”this.$child("banner")“取到子组件后可以获取到其对应data里面对象的值
```javascript
prompt.showToast({
            message:"获得子组件的值为："+this.$child("banner").change,
            duration:3000
        })
```

#### 兄弟组件之间传值
这里将banner组件获取的值，通过父组件作为中转传给了circle组件。除此之外，我们可以采用全局变量、文件存储等实现兄弟组件之间传值。  
1、父组件获取到banner组件的值，将其存在变量index中。
```javascript
getPicIndex(e){
        this.index = e.detail.index;
    }
```
2、将其通过属性方式传给了circle组件。
```html
<circle style="margin-top: 20px;" index="{{index}}"></circle>
```

#### 代码参考
[https://gitee.com/jsntcqy/MyJsComponent](https://gitee.com/jsntcqy/MyJsComponent)  




